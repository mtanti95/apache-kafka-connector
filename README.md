# Kafka Anypoint Connector

Mule Kafka Connector Release Notes

The Anypoint connector for Kafka provides connectivity to the Kafka API. It enables messages to sent and received from the Kafka Broker. 

Version 1.0.0 – June 6, 2016

The Mule Kafka Connector v1.0.0 is compatible with 

	> Mule Runtime 3.7.0 or higher

Supported Operation: 

	> produce
	> consume

 Kafka Connector Demo
======================

INTRODUCTION
------------
  This demo shows how to use the Kafka Connector.
  It shows how to use some operations related with the Kafka API such as producing a message and consuming a message

=== Prerequisites

This document assumes you are familiar with Mule, link:/mule-user-guide/v/3.7/anypoint-connectors[Anypoint Connectors],
and link:/mule-fundamentals/v/3.7/anypoint-studio-essentials[Anypoint Studio Essentials]. To increase your familiarity with Studio,
consider completing one or more link:/mule-fundamentals/v/3.7/basic-studio-tutorial[Anypoint Studio Tutorials]. Further,
this page assumes that you have a basic understanding of link:/mule-fundamentals/v/3.7/elements-in-a-mule-flow[Elements in a Mule Flow] and link:/mule-fundamentals/v/3.7/global-elements[Mule Global Elements].

This document describes implementation examples within the context of Anypoint Studio, Mule ESB’s graphical user interface, and, in parallel,
includes configuration details for doing the same in the XML Editor.

=== Dependencies

For the Kafka Connector to work properly in your Mule application, you need the following:

* A community edition of Anypoint Studio

* Kafka Version 0.10.0 from - https://kafka.apache.org/downloads.html

=== Compatibility Matrix

The Mule Kafka Connector v1.0.0 is compatible with:

* Mule Runtime v3.7.0 or higher


=== Supported Operations:
* consume
* produce

=== Installing
You can install a connector in Anypoint Studio using the instructions in link:/mule-fundamentals/v/3.7/anypoint-exchange#installing-a-connector-from-anypoint-exchange[Installing a Connector from Anypoint Exchange].

=== Updating From an Older Version

Every time an updated version of a connector is released, Anypoint studio displays a popup in the bottom right corner of your screen with the following message: "Updates Available".
To upgrade to the newer version of the Anypoint Mule Kafka Connector:

1. Click the popup and check for available updates.
2. Select the *Anypoint Kafka connector version 1.0 checkbox and click *Next*.
3. Follow the instructions provided by the user interface.
4. Restart Studio when prompted.
After restarting, if you have several versions of the connector installed, Mule asks you for the version of the connector you would like to use.