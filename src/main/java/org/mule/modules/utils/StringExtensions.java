package org.mule.modules.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class StringExtensions {

    /**
     * Parse a String in the form string1,string2 to a collection
     * 
     * @param topics
     *            String in the form string1,string2
     * @return A collection of Strings
     */
    public static Collection<String> parseList(String topics) {
        String trimmed = topics.trim();
        if (trimmed.isEmpty())
            return Collections.emptyList();
        else
            return Arrays.asList(trimmed.split("\\s*,\\s*", -1));
    }
}
