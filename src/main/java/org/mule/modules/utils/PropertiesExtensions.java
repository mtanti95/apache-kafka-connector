package org.mule.modules.utils;

import java.util.Properties;

public class PropertiesExtensions {

    /**
     * Put in the properties the key and the value if is not null or empty
     * 
     * @param properties
     * @param key
     * @param value
     */
    public static void putIfNotNullOrEmpty(Properties properties, String key, String value) {
        if (value != null && !value.isEmpty())
            properties.put(key, value);
    }
}
