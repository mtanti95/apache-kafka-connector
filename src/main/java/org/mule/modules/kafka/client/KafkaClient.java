package org.mule.modules.kafka.client;

import java.io.Closeable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

public class KafkaClient implements Closeable {

    private KafkaProducer<Object, Object> producer;
    private KafkaConsumer<Object, Object> consumer;

    public KafkaClient(KafkaProducer<Object, Object> producer, KafkaConsumer<Object, Object> consumer) {
        this.producer = producer;
        this.consumer = consumer;
    }

    public void send(String topic, Object value) {
        producer.send(new ProducerRecord<Object, Object>(topic, value));
    }

    public void sendAsync(String topic, Object value, Callback callback) {
        producer.send(new ProducerRecord<Object, Object>(topic, value), callback);
    }

    public List<Object> consume(Collection<String> topics) {
        List<Object> messages = new ArrayList<Object>();
        consumer.subscribe(topics);

        ConsumerRecords<Object, Object> records = consumer.poll(1000);
        for (ConsumerRecord<Object, Object> record : records) {
            messages.add(record.value());
        }
        return messages;
    }

    public void close() {
        producer.close();
        consumer.close();
    }
}
