package org.mule.modules.kafka;

import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.clients.producer.Callback;
import org.mule.api.annotations.Config;
import org.mule.api.annotations.Connector;
import org.mule.api.annotations.Processor;
import org.mule.api.annotations.Source;
import org.mule.api.annotations.SourceStrategy;
import org.mule.api.annotations.display.FriendlyName;
import org.mule.api.annotations.display.Summary;
import org.mule.api.annotations.param.Optional;
import org.mule.api.annotations.param.Payload;
import org.mule.api.callback.SourceCallback;
import org.mule.modules.kafka.client.KafkaClient;
import org.mule.modules.kafka.config.ConnectorConfig;
import org.mule.modules.utils.StringExtensions;

@Connector(name = "kafka", friendlyName = "Kafka", minMuleVersion = "3.7")
public class KafkaConnector {

    @Config
    ConnectorConfig config;
    KafkaClient client;

    List<String> previous = new ArrayList<String>();

    @Processor // synchronous
    public void send(String topic, @Payload Object payload) {
        client.send(topic, payload);
    }

    @Processor
    public void sendAsync(String topic, @Payload Object payload, @Optional Callback callback) {
        client.sendAsync(topic, payload, callback);
    }

    @Source(sourceStrategy = SourceStrategy.POLLING, pollingPeriod = 5000)
    public void consume(final SourceCallback callback,
            @Summary("This list should be in the form topic1,topic2,...") @FriendlyName("Topics") String topics)
            throws Exception {
        List<Object> messages = client.consume(StringExtensions.parseList(topics));
        callback.process(messages);
    }

    public ConnectorConfig getConfig() {
        return config;
    }

    public void setConfig(ConnectorConfig config) {
        this.config = config;
        this.client = config.getClient();
    }

}