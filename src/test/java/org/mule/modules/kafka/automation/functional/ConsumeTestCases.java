package org.mule.modules.kafka.automation.functional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ConsumeTestCases extends AbstractTestCases {

    private static final String SOURCE_NAME = "consume";
    private KafkaProducer<Integer, String> producer;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        Object[] signature = {
                null,
                topic
        };
        try {
            getDispatcher().initializeSource(SOURCE_NAME, signature);
        } catch (Throwable e) {

        }
        producer = new KafkaProducer<>(builder.getProducerProperties());
    }

    @After
    public void tearDown() throws Exception {
        producer.close();
        try {
            getDispatcher().shutDownSource(SOURCE_NAME);
        } catch (Throwable e) {
            throw new Exception(e);
        }
    }

    @Test
    public void verify() throws Exception {
        producer.send(new ProducerRecord<Integer, String>(topic, message));

        List<Object> messagesWrapper = getDispatcher().getSourceMessages(SOURCE_NAME);
        assertFalse(messagesWrapper.isEmpty());

        @SuppressWarnings("unchecked")
        List<String> messages = (List<String>) messagesWrapper.get(0);
        assertEquals(message, messages.get(0));
    }

}