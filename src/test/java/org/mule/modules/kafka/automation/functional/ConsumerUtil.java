package org.mule.modules.kafka.automation.functional;

import static org.junit.Assert.assertEquals;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

public class ConsumerUtil {

    private static final long timeoutMs = 5000;
    private static final long polling = 1000;

    public static void checkMessageHasArrived(KafkaConsumer<Integer, String> consumer, String expectedMessage) throws InterruptedException {
        long timeoutExpiredMs = System.currentTimeMillis() + timeoutMs;

        while (System.currentTimeMillis() < timeoutExpiredMs) {
            // we assume we are in a synchronized (object) here
            ConsumerRecords<Integer, String> records = consumer.poll(polling);
            if (!records.isEmpty()) {
                String message = records.iterator()
                        .next()
                        .value();
                assertEquals(expectedMessage, message);
                break;
            }
        }
    }
}
