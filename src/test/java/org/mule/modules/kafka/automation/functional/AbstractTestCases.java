/**
 * (c) 2003-2015 MuleSoft, Inc. This software is protected under international copyright law. All use of this software is subject to MuleSoft�s Master Subscription Agreement (or other Terms of Service) separately entered into between you and MuleSoft. If such an agreement is not in place, you may not use the software.
 */
package org.mule.modules.kafka.automation.functional;

import org.junit.Before;
import org.mule.modules.kafka.KafkaConnector;
import org.mule.tools.devkit.ctf.junit.AbstractTestCase;

public abstract class AbstractTestCases extends AbstractTestCase<KafkaConnector> {

    protected TestDataBuilder builder = new TestDataBuilder();

    protected String topic;
    protected String message;

    public AbstractTestCases() {
        super(KafkaConnector.class);
    }

    @Before
    public void setUp() throws Exception {
        topic = builder.getTopic();
        message = builder.getMessage();
    }
}