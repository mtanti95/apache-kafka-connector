package org.mule.modules.kafka.automation.functional;

import java.util.Properties;

import org.mule.tools.devkit.ctf.configuration.util.ConfigurationUtils;
import org.mule.tools.devkit.ctf.exceptions.ConfigurationLoadingFailedException;

public class TestDataBuilder {

    private String topic = "test";
    private String message = "Hello World!";

    private Properties automationProperties;

    TestDataBuilder() {
        try {
            automationProperties = ConfigurationUtils.getAutomationCredentialsProperties();
        } catch (ConfigurationLoadingFailedException e) {
            e.printStackTrace();
        }
    }

    public String getTopic() {
        return topic;
    }

    public String getMessage() {
        return message;
    }

    public String getAsynchronousMessage() {
        return message + " (Asynchronous)";
    }

    public Properties getConsumerProperties() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", automationProperties.getProperty("config.bootstrapServers"));
        properties.put("group.id", automationProperties.getProperty("config.groupId"));
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        return properties;
    }

    public Properties getProducerProperties() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", automationProperties.getProperty("config.bootstrapServers"));
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        return properties;
    }

}
