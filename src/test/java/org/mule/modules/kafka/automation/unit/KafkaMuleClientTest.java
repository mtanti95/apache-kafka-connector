package org.mule.modules.kafka.automation.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mule.modules.kafka.client.KafkaClient;

public class KafkaMuleClientTest {

    String message = "hello world";
    String topic = "test";

    KafkaProducer<Integer, String> producer;
    KafkaConsumer<Integer, String> consumer;
    private long timeoutMs = 5000;
    private long polling = 100;
    private KafkaClient target;

    @Before
    public void init() {
        consumer = new KafkaConsumer<>(KafkaProperties.getConsumerProperties());
        producer = new KafkaProducer<>(KafkaProperties.getProducerProperties());
        target = new KafkaClient(new KafkaProducer<Object, Object>(KafkaProperties.getProducerProperties()),
                new KafkaConsumer<Object, Object>(KafkaProperties.getConsumerProperties()));
    }

    @After
    public void close() {
        consumer.close();
        producer.close();
        target.close();
    }

    @Test
    public void send() throws InterruptedException {
        consumer.subscribe(Collections.singletonList(topic));

        target.send(topic, this.message);

        checkMessageHasArrived();
    }

    @Test
    public void sendAsync() throws InterruptedException {
        consumer.subscribe(Collections.singletonList(topic));

        target.sendAsync(topic, message, null);

        checkMessageHasArrived();
    }

    @Test
    public void consume_receiveMessage() throws UnsupportedEncodingException {
        KafkaProducer<Integer, String> producer = new KafkaProducer<>(KafkaProperties.getProducerProperties());

        producer.send(new ProducerRecord<Integer, String>(topic, message));
        List<Object> messages = target.consume(Collections.singletonList(topic));
        assertEquals(message, messages.get(0));

        String message2 = "Hello2";
        producer.send(new ProducerRecord<Integer, String>(topic, message));
        producer.send(new ProducerRecord<Integer, String>(topic, message2));
        producer.close();
        messages = target.consume(Collections.singletonList(topic));

        assertEquals(message, messages.get(0));
        assertEquals(message2, messages.get(1));
    }

    @Test
    public void closed() {
        KafkaConsumer<Object, Object> consumer = new KafkaConsumer<>(KafkaProperties.getConsumerProperties());
        KafkaProducer<Object, Object> producer = new KafkaProducer<>(KafkaProperties.getProducerProperties());
        KafkaClient target = new KafkaClient(producer, consumer);

        target.close();

        try {
            consumer.close();
            fail();
        } catch (IllegalStateException e) {
            assertEquals("This consumer has already been closed.", e.getMessage());
        }
    }

    private void checkMessageHasArrived() throws InterruptedException {
        long timeoutExpiredMs = System.currentTimeMillis() + timeoutMs;

        while (System.currentTimeMillis() < timeoutExpiredMs) {
            // we assume we are in a synchronized (object) here
            ConsumerRecords<Integer, String> records = consumer.poll(polling);
            if (!records.isEmpty()) {
                String message = records.iterator()
                        .next()
                        .value();
                assertEquals(this.message, message);
                break;
            }
        }
    }
}
