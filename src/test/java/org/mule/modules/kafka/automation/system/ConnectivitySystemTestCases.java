package org.mule.modules.kafka.automation.system;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mule.api.ConnectionException;
import org.mule.api.ConnectionExceptionCode;
import org.mule.modules.kafka.config.ConnectorConfig;

public class ConnectivitySystemTestCases {

    private ConnectorConfig config;

    @Before
    public void setUp() throws Exception {
        config = new ConnectorConfig();
    }

    @Test
    public void validCredentialsConnectivityTest() {
        assertFalse(config.validateConnection());
        try {
            config.connect("http://localhost:9092");
            assertEquals("1", config.connectionIdentifier());
            assertTrue(config.validateConnection());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void invalidCredentialsConnectivityTest() {
        try {
            // Call the @TestConnectivity
            config.connect("");
        } catch (ConnectionException ce) {
            assertTrue(Arrays.asList(ConnectionExceptionCode.UNKNOWN_HOST, ConnectionExceptionCode.UNKNOWN_HOST)
                    .contains(ce.getCode()));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

}
