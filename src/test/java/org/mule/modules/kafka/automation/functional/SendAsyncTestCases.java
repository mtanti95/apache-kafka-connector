package org.mule.modules.kafka.automation.functional;

import java.util.Collections;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SendAsyncTestCases extends AbstractTestCases {

    private KafkaConsumer<Integer, String> consumer;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        consumer = new KafkaConsumer<>(builder.getConsumerProperties());
        consumer.subscribe(Collections.singletonList(topic));
    }

    @After
    public void tearDown() throws Exception {
        consumer.close();
    }

    @Test
    public void verify() throws InterruptedException {
        getConnector().sendAsync(topic, message, null);

        ConsumerUtil.checkMessageHasArrived(consumer, message);
    }
}